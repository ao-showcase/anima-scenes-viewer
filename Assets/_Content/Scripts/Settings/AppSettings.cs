using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ao
{
    [CreateAssetMenu(fileName = "AppSettings", menuName = "AO/AppSettings")]
    public class AppSettings : ScriptableObject
    {
        [Header("SCENES SEQUENCE")]
        [Tooltip("Scenes to load")] public List<SceneData> ScenesSequence = new List<SceneData>();

        [Header("UI SETTINGS")] 
        [Tooltip("Timeline item")] public UITimelineItem TimelineItemPrefab;
        
        
        // current loaded scene
        [HideInInspector] public int LoadedSceneCounter = 0;
    }
}