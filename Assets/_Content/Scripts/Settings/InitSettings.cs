using System;
using lab.framework;
using UnityEngine;

namespace ao
{
    [Serializable]
    public class InitSettings
    {
        public AppSettings AppSettings;
        
        [HideInInspector] 
        public AsyncProcessor AsyncProcessor;
    }
}