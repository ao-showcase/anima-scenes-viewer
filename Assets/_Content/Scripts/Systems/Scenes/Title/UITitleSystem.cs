using System.Collections;
using lab.framework;
using UnityEngine;

namespace ao
{
    /// <summary>
    /// System for title demonstration in first scene
    /// </summary>
    public class UITitleSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        
        private UITitle uiTitle;
        private static readonly int showParam = Animator.StringToHash("Show");

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiTitle = GameObject.FindObjectOfType<UITitle>();

            if (!uiTitle)
                return;

            InitSettings.AsyncProcessor.StartCoroutine(RunTitleTimer());
        }

        private IEnumerator RunTitleTimer()
        {
            uiTitle.Animator.SetBool(showParam, true);
            
            yield return new WaitForSeconds(2);
            
            uiTitle.Animator.SetBool(showParam, false);
            
            appEventsSystem.OnLoadNextSceneRequest?.Invoke();
        }
    }
}