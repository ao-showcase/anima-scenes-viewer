using lab.framework;
using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    /// <summary>
    /// System for scenario restart
    /// </summary>
    public class UIRestartButtonsSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            Button restartButton = GameObject.FindObjectOfType<Button>();
            restartButton.onClick.AddListener(OnRestartClicked);
        }

        private void OnRestartClicked()
        {
            appEventsSystem.OnLoadNextSceneRequest.Invoke();
        }
    }
}