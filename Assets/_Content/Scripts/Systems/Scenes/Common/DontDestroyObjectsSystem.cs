using System.Collections.Generic;
using lab.framework;
using UnityEditor;
using UnityEngine;

namespace ao
{
    public class PassedObjects
    {
        public readonly List<Appearance> Objects = new List<Appearance>();
    }
    
    /// <summary>
    /// System for passing objects to another scene
    /// </summary>
    public class DontDestroyObjectsSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        
        private Dictionary<string, PassedObjects> objectsByScenes = new Dictionary<string, PassedObjects>();
        private static readonly int showParam = Animator.StringToHash("Show");

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            appEventsSystem.OnAppearanceClicked += PassObject;
            appEventsSystem.OnPassedResources += OnUnloadUnusedResources;
        }

        /// <summary>
        /// Unload resources of scene with a name
        /// </summary>
        /// <param name="sceneName"></param>
        private void OnUnloadUnusedResources(string sceneName)
        {
            if (objectsByScenes.ContainsKey(sceneName))
            {                
                foreach (var obj in objectsByScenes[sceneName].Objects)
                {
                    obj.Animator.SetBool(showParam, false);
                }
                objectsByScenes[sceneName].Objects.Clear();
            }
        }

        /// <summary>
        /// Pass object to another scene
        /// </summary>
        /// <param name="appearance"></param>
        private void PassObject(Appearance appearance)
        {
            GameObject.DontDestroyOnLoad(appearance.gameObject);

            string sceneName = InitSettings.AppSettings.ScenesSequence[InitSettings.AppSettings.LoadedSceneCounter].SceneAsset.name;

            if (objectsByScenes.ContainsKey(sceneName))
            {
                objectsByScenes[sceneName].Objects.Add(appearance);
            }
            else
            {
                PassedObjects passedObjects = new PassedObjects();
                passedObjects.Objects.Add(appearance);
                
                objectsByScenes.Add(sceneName, passedObjects);
            }
            
            appEventsSystem.OnLoadNextSceneRequest?.Invoke();
        }
    }
}