using System.Collections.Generic;
using lab.framework;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ao
{
    /// <summary>
    /// System for animated appearance and death of objects
    /// </summary>
    public class AppearanceSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        
        private List<Appearance> appearances = new List<Appearance>();
        private static readonly int showParam = Animator.StringToHash("Show");

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            appEventsSystem.OnSceneLoaded += OnLoadResources;
            appEventsSystem.OnUnloadResources += OnUnloadResources;
        }

        /// <summary>
        /// Save controlled objects of loaded scene into list
        /// </summary>
        /// <param name="scene"></param>
        private void OnLoadResources(Scene scene, int sceneNum)
        {
            GameObject[] objects = scene.GetRootGameObjects();

            foreach (var obj in objects)
            {
                Appearance appearance = obj.GetComponent<Appearance>();

                if (appearance)
                {
                    appearances.Add(appearance);
                }
            }
        }

        /// <summary>
        /// Unload controlled objects
        /// </summary>
        private void OnUnloadResources()
        {
            foreach (var appearance in appearances)
            {
                if (!appearance.Keep)
                {
                    appearance.Animator.SetBool(showParam, false);
                }
            }
            
            appearances.Clear();
        }
    }
}