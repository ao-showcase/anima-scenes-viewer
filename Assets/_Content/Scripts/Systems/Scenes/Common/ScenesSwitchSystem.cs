using System.Collections;
using lab.framework;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ao
{
    /// <summary>
    /// System for switching between scenes
    /// </summary>
    public class ScenesSwitchSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;

        private int curSceneNum = 0;
        
        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            
            appEventsSystem.OnLoadNextSceneRequest += LoadNextScene;
            LoadScene(curSceneNum);
            
            SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
        }

        private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            appEventsSystem.OnSceneLoaded?.Invoke(scene, curSceneNum);
        }

        private void LoadNextScene()
        {
            int prevSceneNum = curSceneNum - 1;
            if (prevSceneNum < 0)
            {
                prevSceneNum = InitSettings.AppSettings.ScenesSequence.Count - 1;
            }
            
            // unload objects that are not used anymore 
            appEventsSystem.OnPassedResources?.Invoke(InitSettings.AppSettings.ScenesSequence[prevSceneNum].SceneAsset.name);
            
            // unload objects from previous scene
            appEventsSystem.OnUnloadResources?.Invoke();
            
            // unload current scene
            InitSettings.AsyncProcessor.StartCoroutine(UnloadSceneByTimer(curSceneNum));
            
            // load next scene
            curSceneNum++;
            LoadScene(curSceneNum);
        }

        private void LoadScene(int sceneNum)
        {
            if (curSceneNum == InitSettings.AppSettings.ScenesSequence.Count)
            {
                curSceneNum = 0;
            }

            InitSettings.AppSettings.LoadedSceneCounter = curSceneNum;
            
            SceneManager.LoadScene(InitSettings.AppSettings.ScenesSequence[curSceneNum].SceneAsset.name, LoadSceneMode.Additive);
        }

        private IEnumerator UnloadSceneByTimer(int sceneNum)
        {
            yield return new WaitForSeconds(InitSettings.AppSettings.ScenesSequence[sceneNum].UnloadDelay);
            SceneManager.UnloadSceneAsync(InitSettings.AppSettings.ScenesSequence[sceneNum].SceneAsset.name);
        }
    }
}