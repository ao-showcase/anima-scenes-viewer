using lab.framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ao
{
    /// <summary>
    /// Global events aggregator for application
    /// </summary>
    public class AppEventsSystem : BaseInitializableMonobehaviour
    {
        // event is called when next scene need to be loaded
        public UnityAction OnLoadNextSceneRequest;
        // event is called when appearance object is clicked
        public UnityAction<Appearance> OnAppearanceClicked;
        // event is called when we need to pass some object to the next scene
        public UnityAction<string> OnPassedResources;
        // event is called when we need to unload objects from previous scene
        public UnityAction OnUnloadResources;
        // event is called when scene is loaded
        public UnityAction<Scene, int> OnSceneLoaded;

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);
            DontDestroyOnLoad(gameObject);
        }
    }
}