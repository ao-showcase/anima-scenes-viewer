using System.Collections.Generic;
using lab.framework;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ao
{
    /// <summary>
    /// Generative timeline system
    /// </summary>
    public class TimelineSystem : BaseInitializableSystem
    {
        [Inject] private AppEventsSystem appEventsSystem;
        private UITimelinePanel uiTimelinePanel;
        
        private List<UITimelineItem> timelineItems = new List<UITimelineItem>();
        private int curScene = 0;
        private static readonly int showParam = Animator.StringToHash("Show");

        public override void Initialize(params object[] arg)
        {
            base.Initialize(arg);

            uiTimelinePanel = GameObject.FindObjectOfType<UITimelinePanel>();

            if (!uiTimelinePanel)
                return;
            
            GenerateTimeline();
            appEventsSystem.OnSceneLoaded += OnSceneLoaded;
        }

        private void GenerateTimeline()
        {
            for (int i = 0; i < InitSettings.AppSettings.ScenesSequence.Count; i++)
            {
                UITimelineItem item = GameObject.Instantiate(InitSettings.AppSettings.TimelineItemPrefab, uiTimelinePanel.transform, true);
                item.Text.text = "" + (i + 1);
                
                timelineItems.Add(item);
            }
        }
        
        private void OnSceneLoaded(Scene scene, int sceneNum)
        {
            timelineItems[curScene].Animator.SetBool(showParam, false);
            timelineItems[sceneNum].Animator.SetBool(showParam, true);
            curScene = sceneNum;
        }
    }
}