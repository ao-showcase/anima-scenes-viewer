using lab.framework;
using UnityEngine;

namespace ao
{
    public class BaseInitializableSystem: IInitializable
    {
        protected InitSettings InitSettings;
        
        public virtual void Initialize(params object[] arg)
        {
            if (arg.Length == 0)
                return;
            
            InitSettings = arg[0] as InitSettings;
            if (InitSettings == null)
            {
                Debug.LogError("No InitSettings");
                return;
            }
        }
    }
}