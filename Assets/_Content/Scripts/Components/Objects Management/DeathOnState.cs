using UnityEngine;

namespace ao
{
    /// <summary>
    /// Object will be destroyed when entered the state
    /// </summary>
    public class DeathOnState : StateMachineBehaviour
    {
        [SerializeField] private string deathState;

        private void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
        {
            if (animatorStateInfo.IsName(deathState))
            {
                Destroy(animator.gameObject);
            }
        }
    }
}