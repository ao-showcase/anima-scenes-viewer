using UnityEngine;

namespace ao
{
    /// <summary>
    /// Component to add on objects that should appear/die with animation, or be passed between scenes
    /// </summary>
    public class Appearance : MonoBehaviour
    {
        public Animator Animator;
        public bool Keep = false;

        /// <summary>
        /// Save object for next scene
        /// </summary>
        public void KeepObject()
        {
            Keep = true;
        }
    }
}