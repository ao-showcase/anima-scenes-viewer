using UnityEngine;
using UnityEngine.Events;

namespace ao
{
    /// <summary>
    /// Clickable appearance
    /// </summary>
    public class ClickableAppearance : MonoBehaviour
    {
        [SerializeField] private UnityEvent OnClick;
        [SerializeField] private Appearance parent;

        private AppEventsSystem appEventsSystem;
        private bool clicked = false;

        private void Awake()
        {
            appEventsSystem = FindObjectOfType<AppEventsSystem>();
        }

        private void OnMouseDown()
        {
            if (!parent || !appEventsSystem || clicked)
                return;
            
            parent.Keep = true;
            OnClick?.Invoke();
            appEventsSystem.OnAppearanceClicked?.Invoke(parent);
           
            clicked = true;
        }
    }
}