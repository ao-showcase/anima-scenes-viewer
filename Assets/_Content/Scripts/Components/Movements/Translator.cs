using UnityEngine;

namespace ao
{
    /// <summary>
    /// Transition component
    /// </summary>
    public class Translator : MovementBehaviour
    {
        [SerializeField] private Transform moveTo;
        [SerializeField] private float velocity;

        private bool transition = false;
        private float startTime;
        private float transitionLength;
        private Vector3 startPos;

        public void StartTransition()
        {
            DisableAnotherMovement();
            transition = true;
            startTime = Time.time;
            startPos = transform.position;
            transitionLength = Vector3.Distance(startPos, moveTo.position);
        }

        private void Update()
        {
            if (!moveTo)
                return;
            
            if (transition)
            {
                float elapsedDist = (Time.time - startTime) * velocity;
                float fracJourney = elapsedDist / transitionLength;
                transform.position = Vector3.Lerp(startPos, moveTo.position, fracJourney);
            }
        }
    }
}