using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ao
{
    /// <summary>
    /// Parent class for spheres movements
    /// </summary>
    public class MovementBehaviour : MonoBehaviour
    {
        protected List<MovementBehaviour> movementBehaviours = new List<MovementBehaviour>();

        protected virtual void Awake()
        {
            movementBehaviours = GetComponentsInChildren<MovementBehaviour>().ToList();
        }

        protected virtual void DisableAnotherMovement()
        {
            foreach (var movement in movementBehaviours)
            {
                if (movement != this)
                {
                    movement.Disable();
                }
            }
        }

        public void Disable()
        {
            enabled = false;
        }
    }
}