using UnityEngine;

namespace ao
{
    /// <summary>
    /// Rotator components
    /// </summary>
    public class Rotator : MovementBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float orbitDistance = 10.0f;
        [SerializeField] private float orbitDegreesPerSec = 180.0f;
        [SerializeField] private Vector3 axis;
         
        private void LateUpdate() 
        {
            if (!target)
                return;
            
            transform.position = target.position + (transform.position - target.position).normalized * orbitDistance;
            transform.RotateAround(target.position, axis, orbitDegreesPerSec * Time.deltaTime);
        }
    }
}