using UnityEngine;

namespace ao
{
    /// <summary>
    /// Title marker
    /// </summary>
    public class UITitle : MonoBehaviour
    {
        public Animator Animator;
    }
}