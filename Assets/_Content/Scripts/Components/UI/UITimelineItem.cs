using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    public class UITimelineItem : MonoBehaviour
    {
        public Text Text;
        public Animator Animator;
    }
}