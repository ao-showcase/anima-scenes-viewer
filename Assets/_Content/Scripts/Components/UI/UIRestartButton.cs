using UnityEngine;
using UnityEngine.UI;

namespace ao
{
    [RequireComponent(typeof(Button))]
    public class UIRestartButton : MonoBehaviour
    {
        // just marker type of restart button
        // to use it instead of tag
    }
}