using System;
using UnityEditor;

namespace ao
{
    /// <summary>
    /// Scene data for scenes sequence/loading
    /// </summary>
    [Serializable]
    public class SceneData
    {
        public SceneAsset SceneAsset;
        public float UnloadDelay;
    }
}