using lab.framework;
using UnityEngine;

namespace ao
{
    /// <summary>
    /// Spheres scene bootstrap
    /// </summary>
    public class SpheresSceneBootstrap : BootstrapBase
    {
        // settings passed to all systems
        [SerializeField] private InitSettings initSettings;

        // event aggregator
        private AppEventsSystem appEventsSystem;

        protected override void Awake()
        {
            base.Awake();

            initSettings.AsyncProcessor = FindObjectOfType<AsyncProcessor>();

            appEventsSystem = FindObjectOfType<AppEventsSystem>();
            systemsContainer.RegisterInstance(appEventsSystem);

            systemsContainer.ResolveAll();
            systemsContainer.InitializeAll(initSettings);
            systemsContainer.LateInitializeAll();
        }
    }
}