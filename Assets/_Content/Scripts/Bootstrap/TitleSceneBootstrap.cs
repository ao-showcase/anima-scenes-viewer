using lab.framework;
using UnityEngine;

namespace ao
{
    /// <summary>
    /// Title scene bootstrap
    /// </summary>
    public class TitleSceneBootstrap : BootstrapBase
    {
        // settings passed to all systems
        [SerializeField] private InitSettings initSettings;
        
        // event aggregator
        private AppEventsSystem appEventsSystem;
        // title demo system
        private UITitleSystem uiTitleSystem;
        
        protected override void Awake()
        {
            base.Awake();

            initSettings.AsyncProcessor = GameObject.FindObjectOfType<AsyncProcessor>();
            
            appEventsSystem = FindObjectOfType<AppEventsSystem>();
            systemsContainer.RegisterInstance(appEventsSystem);
            
            uiTitleSystem = systemsContainer.Register(typeof(UITitleSystem));
            
            systemsContainer.ResolveAll();
            systemsContainer.InitializeAll(initSettings);
            systemsContainer.LateInitializeAll();
        }
    }
}