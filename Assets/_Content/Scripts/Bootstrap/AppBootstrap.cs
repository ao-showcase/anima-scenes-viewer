using lab.framework;
 using UnityEngine;
 
 namespace ao
 {
     /// <summary>
     /// Common scene bootstrap
     /// </summary>
     public class AppBootstrap : BootstrapBase
     {    
         // settings passed to all systems
         [SerializeField] private InitSettings initSettings;
 
         // event aggregator
         private AppEventsSystem appEventsSystem;
         // scenes switcher
         private ScenesSwitchSystem scenesSwitchSystem;
         // system for passing objects between scenes
         private DontDestroyObjectsSystem dontDestroyObjectsSystem;
         // system for animated appearance and death of objects
         private AppearanceSystem appearanceSystem;
         // system for timeline
         private TimelineSystem timelineSystem;
        
         protected override void Awake()
         {
             base.Awake();
 
             initSettings.AsyncProcessor = CreateAsyncProcessor();
 
             GameObject eventsHolder = new GameObject("App events");
             appEventsSystem = eventsHolder.AddComponent<AppEventsSystem>();
             systemsContainer.RegisterInstance(appEventsSystem);
             
             scenesSwitchSystem = systemsContainer.Register(typeof(ScenesSwitchSystem));
             dontDestroyObjectsSystem = systemsContainer.Register(typeof(DontDestroyObjectsSystem));
             appearanceSystem = systemsContainer.Register(typeof(AppearanceSystem));
             timelineSystem = systemsContainer.Register(typeof(TimelineSystem));
 
             systemsContainer.ResolveAll();
             systemsContainer.InitializeAll(initSettings);
             systemsContainer.LateInitializeAll();
         }
     }
 }