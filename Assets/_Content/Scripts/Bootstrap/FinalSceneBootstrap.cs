using lab.framework;
using UnityEngine;

namespace ao
{
    /// <summary>
    /// Final scene bootstrap
    /// </summary>
    public class FinalSceneBootstrap : BootstrapBase
    {
        // settings passed to all systems
        [SerializeField] private InitSettings initSettings;

        // event aggregator
        private AppEventsSystem appEventsSystem;
        // ui system
        private UIRestartButtonsSystem uiRestartButtonsSystem;

        protected override void Awake()
        {
            base.Awake();

            initSettings.AsyncProcessor = FindObjectOfType<AsyncProcessor>();

            appEventsSystem = FindObjectOfType<AppEventsSystem>();
            systemsContainer.RegisterInstance(appEventsSystem);

            uiRestartButtonsSystem = systemsContainer.Register(typeof(UIRestartButtonsSystem));

            systemsContainer.ResolveAll();
            systemsContainer.InitializeAll(initSettings);
            systemsContainer.LateInitializeAll();
        }
    }
}